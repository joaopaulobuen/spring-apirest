package com.produtos.apirest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.produtos.apirest.models.Moeda;

public interface MoedaJpaRepository extends JpaRepository<Moeda, Long> {


}
