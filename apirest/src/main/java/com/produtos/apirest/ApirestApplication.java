package com.produtos.apirest;

import java.net.URISyntaxException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

//import com.produtos.apirest.client.RestClientUtil;

@SpringBootApplication
@EnableScheduling
public class ApirestApplication {

	public static void main(String[] args) throws URISyntaxException {
		SpringApplication.run(ApirestApplication.class, args);
		
			//RestClientUtil util = new RestClientUtil();
			//util.getEmployeeDemo1();
		
	}

}
