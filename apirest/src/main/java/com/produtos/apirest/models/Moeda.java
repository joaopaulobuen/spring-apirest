package com.produtos.apirest.models;

import java.io.Serializable;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name = "ad_ctmdolar")
public class Moeda implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(generator="seqdolar")
	@SequenceGenerator(name="seqdolar", sequenceName = "sq_dolar", initialValue = 1, allocationSize=1)
	@Column (name = "sequencia")
	private Integer id;
	@Column (name = "codigo")
	private Integer codigo = 1;
	@Transient
	private String value;
	@Column (name = "vlrdolar")
	private Double cotacaoVenda;
	@Column (name = "dtcotacao")
	private String dataHoraCotacao;

	
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	public Double getCotacaoVenda() {
		return cotacaoVenda;
	}
	public void setCotacaoVenda(Double cotacaoVenda) {
		this.cotacaoVenda = cotacaoVenda;
	}
	public String getDataHoraCotacao() {
		return dataHoraCotacao;
	}
	public void setDataHoraCotacao(String dataHoraCotacao) {
		this.dataHoraCotacao = dataHoraCotacao;
	}
	
	
}
