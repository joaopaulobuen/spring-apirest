package com.produtos.apirest.models;


import java.io.Serializable;
import java.util.List;




//import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MainMoeda implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Transient
	private List<Valor> value;


	public List<Valor> getValue() {
		return value;
	}

	public void setValue(List<Valor> value) {
		this.value = value;
	}

}