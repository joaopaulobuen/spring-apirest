package com.produtos.apirest.models;


import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Valor implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String cotacaoVenda;
	private String dataHoraCotacao;
	
	public String getCotacaoVenda() {
		return cotacaoVenda;
	}
	public void setCotacaoVenda(String cotacaoVenda) {
		this.cotacaoVenda = cotacaoVenda;
	}
	public String getDataHoraCotacao() {
		return dataHoraCotacao;
	}
	public void setDataHoraCotacao(String dataHoraCotacao) {
		this.dataHoraCotacao = dataHoraCotacao;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
}
