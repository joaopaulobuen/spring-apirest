package com.produtos.apirest.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.produtos.apirest.models.Moeda;
import com.produtos.apirest.models.MainMoeda;
import com.produtos.apirest.repository.MoedaJpaRepository;


@RestController
@RequestMapping("/moedas")
@Component
public class MoedaController {
	
	
	@Autowired
	private MoedaJpaRepository moedaJpaRepository;
	
	@GetMapping(value = "/saveapimoeda")
	@Scheduled (cron = "0 0 15 * * 1-5")
	public void save() throws URISyntaxException {
		
		try {
			String dataHoje;
			Date data = new Date();
			SimpleDateFormat formatador = new SimpleDateFormat("MM-dd-yyyy");
			
			
			dataHoje = formatador.format(data);
			
			
			System.out.println(dataHoje);
			
			
			URI uri = new URI("https://olinda.bcb.gov.br/olinda/servico/PTAX/versao/v1/odata/CotacaoMoedaPeriodoFechamento(codigoMoeda=@codigoMoeda,dataInicialCotacao=@dataInicialCotacao,dataFinalCotacao=@dataFinalCotacao)?@codigoMoeda='USD'&@dataInicialCotacao='"+ dataHoje +"'&@dataFinalCotacao='"+ dataHoje + "'&$format=json&$select=cotacaoVenda,dataHoraCotacao");
			
			System.out.println(uri);
			
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<MainMoeda> responseEntity = restTemplate.getForEntity(uri, MainMoeda.class);
			MainMoeda main = responseEntity.getBody();
			
			Moeda moeda = new Moeda();
				
				//Formata para definir a quantidade de casas decimais			
				//DecimalFormat df =  new DecimalFormat("0.00");
					
				//Teste do valor a ser inserido
				//System.out.println(cotacao);
				
			
				//Insere os valores convertidos no banco de dados
				moeda.setCotacaoVenda(Double.parseDouble(main.getValue().get(0).getCotacaoVenda()));
				moeda.setDataHoraCotacao(main.getValue().get(0).getDataHoraCotacao());

				//Salva os valores no banco de dados			
			    moedaJpaRepository.save(moeda);
			    moedaJpaRepository.flush();

		} catch (Exception e) {
			System.err.println("Não foi possível obter os dados requisitados da API, verificar dados e link utilizado para consulta. ATENÇÃO!! Os dados só poderão ser obtidos em dias em que o Banco Central diposnibilizar as informações para requisição.");
			System.err.println("Nenhum dado foi inserido no banco.");
		}
			
	}
	
}
