package com.produtos.apirest.client;
import com.produtos.apirest.models.MainMoeda;

import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class RestClientUtil {
	
	public void getEmployeeDemo1() throws URISyntaxException {
		
		URI uri = new URI("https://olinda.bcb.gov.br/olinda/servico/PTAX/versao/v1/odata/CotacaoMoedaPeriodoFechamento(codigoMoeda=@codigoMoeda,dataInicialCotacao=@dataInicialCotacao,dataFinalCotacao=@dataFinalCotacao)?@codigoMoeda='USD'&@dataInicialCotacao='11-25-2020'&@dataFinalCotacao='11-25-2020'&$format=json&$select=cotacaoVenda,dataHoraCotacao");
		
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<MainMoeda> responseEntity = restTemplate.getForEntity(uri, MainMoeda.class);

		System.out.println("Status Code: " + responseEntity.getStatusCode());
		MainMoeda prod = responseEntity.getBody();
			
			
		System.out.println(prod.getValue().get(0).getCotacaoVenda() + " " + prod.getValue().get(0).getDataHoraCotacao());
	}
	
}
	